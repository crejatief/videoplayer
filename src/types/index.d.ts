interface videoEvent {
  id: string;
  time: number;
  type: string;
  [key: string]: any;
}
interface tickerItem {
  id: string;
  body: string;
}
