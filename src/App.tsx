import React from 'react';
import VideoPlayer from './components/VideoPlayer';

function App() {
  return (
    <div className="App">
      <VideoPlayer videoId="1f515350-8bbb-11ea-b153-19d34a45fe73" />
    </div>
  );
}

export default App;
