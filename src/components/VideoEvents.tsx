import React from 'react';
import { VideoEvent } from './elements/VideoEvent';

interface IState {
  visibleEvents: videoEvent[];
}
interface IProps {
  currentTime: number;
  events: videoEvent[];
}

const EVENT_DURATION = 5;

export default class VideoEvents extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      visibleEvents: [],
    }
    this.checkVisibleEvents = this.checkVisibleEvents.bind(this);
  }
  checkVisibleEvents() {
    const { events, currentTime } = this.props;
    const { visibleEvents } = this.state;

    const activeEvents = events.filter(e => e.time < currentTime && currentTime < (e.time + EVENT_DURATION));
    const [visibleIds, activeIds] = [visibleEvents.map(e => e.id), activeEvents.map(e => e.id)];
    if (activeIds.every(e => visibleIds.includes(e)) && activeIds.length === visibleIds.length) {
      return;
    }
    this.setState({
      visibleEvents: activeEvents.sort((a, b) => a.time - b.time),
    });
  }
  componentDidUpdate() {
    this.checkVisibleEvents();
  }
  render() {
    const { events } = this.props;
    const { visibleEvents } = this.state;
    return (
      <ul className="video-events">
        { events.map(e =>
          <VideoEvent isVisible={visibleEvents.some(ve => e.id === ve.id)} event={e} key={e.id} />
        ) }
      </ul>
    );
  }
}