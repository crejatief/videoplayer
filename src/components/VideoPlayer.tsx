import React from 'react';
import { fetchVideoEvents } from '../lib/Api';

import VideoEvents from './VideoEvents';
import VideoTicker from './VideoTicker';

interface videoEventsResponse {
  ticker?: tickerItem[];
  events?: videoEvent[];
};
interface IState {
  currentTime: number;
  ticker?: tickerItem[];
  events?: videoEvent[];
}
interface IProps {
  videoId: string;
}

export default class VideoPlayer extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      currentTime: 0,
    };
    this.timeUpdate = this.timeUpdate.bind(this);
    this.initPlayer = this.initPlayer.bind(this);
  }
  timeUpdate(event: any) {
    const { currentTime } = event.target;
    this.setState({
      currentTime,
    });
  }
  async initPlayer() {
    const { ticker, events } : videoEventsResponse = await fetchVideoEvents(this.props.videoId);
    this.setState({
      events,
      ticker,
    });
  }
  componentDidMount() {
    this.initPlayer();
  }
  render() {
    const { videoId } = this.props;
    const { events, ticker, currentTime } = this.state;
    return(
      <div className="video-player">
        <div className="video-player__wrapper">
          <video className="video-player__video" src={`/media/${videoId}/video.mp4`} controls onTimeUpdate={this.timeUpdate}></video>
          { events ? <VideoEvents events={events} currentTime={currentTime} /> : null }
          { ticker ? <VideoTicker items={ticker} currentTime={currentTime} /> : null }
        </div>
      </div>
    );
  }
};
