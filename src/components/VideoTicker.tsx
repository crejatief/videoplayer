import React from 'react';

interface IProps {
  currentTime: number;
  items: tickerItem[];
}

const TICKERITEM_DURATION = 4;

export default class VideoTicker extends React.Component<IProps, {}> {
  constructor(props: IProps) {
    super(props);

    this.getClassName = this.getClassName.bind(this);
  }
  getClassName(itemId: string) {
    const { currentTime, items } = this.props;

    const tickerCycles = Math.floor(currentTime / TICKERITEM_DURATION) + 1;
    const amountOfCycles = Math.ceil(tickerCycles / items.length) - 1;
    const activeItemIndex = (tickerCycles - (items.length * amountOfCycles) - 1);

    const { id: activeId } = items[activeItemIndex];
    return (activeId === itemId) ? 'video-ticker__item is-active' : 'video-ticker__item';
  }
  render() {
    const { items } = this.props;
    return (
      <ul className="video-ticker">
        { items.map(i => <li className={this.getClassName(i.id)} key={i.id}>{i.body}</li>) }
      </ul>
    );
  }
}
