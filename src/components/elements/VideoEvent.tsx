import React from 'react';
import { EventTypeGoal } from './events/EventTypeGoal';
import { EventTypeHalfTime } from './events/EventTypeHalfTime';
import { EventTypeCard } from './events/EventTypeCard';

interface IProps {
  event: videoEvent;
  isVisible: boolean;
}

const RIGHT_EVENTS = ['endHalf'];

const getEnrichedClasses = (eventType: string, isVisible: boolean) => {
  const extraClasses: string[] = [];
  if (RIGHT_EVENTS.includes(eventType)) extraClasses.push('is-right');
  if (isVisible) extraClasses.push('is-active');
  return extraClasses.join(' ');
};

const renderEventByType = (event: videoEvent) => {
  if (event.type === 'goal') return <EventTypeGoal event={event} />
  if (event.type === 'endHalf') return <EventTypeHalfTime event={event} />
  if (event.type === 'card') return <EventTypeCard event={event} />
  return;
};

export const VideoEvent: React.SFC<IProps> = (props: IProps) => {
  const { event, isVisible } = props;
  return (
    <li className={`video-events__event ${getEnrichedClasses(event.type, isVisible)}`}>
      { renderEventByType(event) }
    </li>
  )
}