import React from 'react';

interface IProps {
  event: videoEvent;
}

export const EventTypeHalfTime: React.SFC<IProps> = (props: IProps) => {
  const { event } = props;
  return (
    <div className="event-type__default">
      <div className="event-type__default--top">
        <small>End half</small>
        <span>{ event.time }</span>
      </div>
    </div>
  )
}