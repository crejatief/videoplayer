import React from 'react';

interface IProps {
  event: videoEvent;
}

export const EventTypeGoal: React.SFC<IProps> = (props: IProps) => {
  const { event } = props;
  return (
    <div className="event-type__default">
      <div className="event-type__default--top">
        <small>Goal!!</small>
        <span>"{ event.time }</span>
      </div>
      <div className="event-type__default--content">
        <p className="event-type__default--by">{event.player}</p>
        <p className="event-type__default--score">{ event.newScore.home }-{event.newScore.away}</p>
      </div>
    </div>
  )
}