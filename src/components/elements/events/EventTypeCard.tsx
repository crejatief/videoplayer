import React from 'react';

interface IProps {
  event: videoEvent;
}

export const EventTypeCard: React.SFC<IProps> = (props: IProps) => {
  const { event } = props;
  return (
    <div className="event-type__default">
      <div className="event-type__default--top">
        <small>{event.cardType} Card</small>
        <span>"{ event.time }</span>
      </div>
      <div className="event-type__default--content">
        <p className="event-type__default--by">{event.player}</p>
      </div>
    </div>
  )
}