import axios from 'axios';

export const fetchVideoEvents = (videoId: string) => axios.get(`https://jsonblob.com/api/jsonBlob/${videoId}`).then(res => res.data);
